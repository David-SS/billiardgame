﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBallController : MonoBehaviour {

    public GameObject whiteBall;
    public Transform whiteBallPosition;
    private GameObject[] balls = new GameObject[15];
    public Transform firstBallPosition;

    private float xPositionIncrement = 1.5f;
    private float zPositionIncrement = 1.4f;

	void Start () {
        loadBallPrefabs();
        resetWhiteBall();
        resetBalls();
	}

    private void loadBallPrefabs()
    {
        balls[0] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_01.prefab", typeof(Object)) as GameObject;
        balls[1] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_03.prefab", typeof(Object)) as GameObject;
        balls[2] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_10.prefab", typeof(Object)) as GameObject;
        balls[3] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_13.prefab", typeof(Object)) as GameObject;
        balls[4] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_08.prefab", typeof(Object)) as GameObject;
        balls[5] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_06.prefab", typeof(Object)) as GameObject;
        balls[6] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_02.prefab", typeof(Object)) as GameObject;
        balls[7] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_15.prefab", typeof(Object)) as GameObject;
        balls[8] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_04.prefab", typeof(Object)) as GameObject;
        balls[9] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_09.prefab", typeof(Object)) as GameObject;
        balls[10] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_07.prefab", typeof(Object)) as GameObject;
        balls[11] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_12.prefab", typeof(Object)) as GameObject;
        balls[12] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_05.prefab", typeof(Object)) as GameObject;
        balls[13] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_11.prefab", typeof(Object)) as GameObject;
        balls[14] = UnityEditor.AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Billiard Balls/Ball_14.prefab", typeof(Object)) as GameObject;
    }

    public void resetWhiteBall()
    {
        whiteBall.GetComponent<Rigidbody>().isKinematic = true;
        whiteBall.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
        whiteBall.transform.position = whiteBallPosition.position;
        whiteBall.GetComponent<Rigidbody>().isKinematic = false;
    }

    public void resetBalls() {
        int ballsOnRow = 1;
        int currentBall= 0;
        Transform nextTrasformBall = firstBallPosition;
        for(int i = 0; i < 5; i++) {
            for(int j = 0; j < ballsOnRow; j++)
            {
                Vector3 currentBallPosition = new Vector3(
                    nextTrasformBall.position.x,
                    nextTrasformBall.position.y,
                    nextTrasformBall.position.z + zPositionIncrement * j
                );
                Instantiate(balls[currentBall], currentBallPosition, Quaternion.identity);
                currentBall++;
            }
            ballsOnRow++;
            nextTrasformBall.position = new Vector3(
                nextTrasformBall.position.x - xPositionIncrement, 
                nextTrasformBall.position.y,
                nextTrasformBall.position.z - zPositionIncrement/2
            );
        }
    }
}
