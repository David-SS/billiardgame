﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBallController : MonoBehaviour {

    private float gravity = 140f; //Min=55f
    private float limitToApplyGravity = 0.675f;

    void Update () {
        if (this.transform.position.y > limitToApplyGravity) setGravityOverBalls();
	}

    private void setGravityOverBalls()
    {
        Rigidbody rball = this.GetComponent<Rigidbody>();
        rball.AddForce(Vector3.down * gravity * rball.mass);
    }
}