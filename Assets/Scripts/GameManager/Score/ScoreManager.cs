﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public const int NONE = -1;
    public const int SOLID = 0;
    public const int STRIPPED = 1;

    public static int player1BallType = NONE;
    public static int player2BallType = NONE;

    public static int player1Balls = 7;
    public static int player2Balls = 7;

    public static int player1FinalHole = NONE;
    public static int player2FinalHole = NONE;

    public static int availableHits = 1;
    public static bool isPlayer1 = true;
    public static bool player1Win = false;
    public static bool player2Win = false;
    private static bool whiteBallOnHole = false;
    private static bool ilegalBallOnHole = false;

    void Start()
    {
        player1BallType = NONE;
        player2BallType = NONE;
        player1Balls = 7;
        player2Balls = 7;
        player1FinalHole = NONE;
        player2FinalHole = NONE;
        availableHits = 1;
        isPlayer1 = true;
        player1Win = false;
        player2Win = false;
        whiteBallOnHole = false;
        ilegalBallOnHole = false;
    }

    void Update () {
        if (whiteBallOnHole) {
            whiteBallOnHole = false;
            GetComponent<SpawnBallController>().resetWhiteBall();
        }
        if (availableHits < 1) changeCurrentPlayer();
    }

    public static void ballOnHole(string ballType, GameObject hole)
    {
        switch (ballType)
        {
            case "BlackBall":
                endOfGame(hole);
                break;
            case "WhiteBall":
                whiteBallOnHole = true;
                ilegalBallOnHole = true;
                break;
            default:
                isMyBallType(ballType, hole);
                break;
        }        
    }

    public static void checkTurn() 
    {
        if (availableHits > 2) availableHits = 2;
        availableHits--;
        if (ilegalBallOnHole) changeCurrentPlayer();
    }

    private static void changeCurrentPlayer()
    {
        availableHits = 1;
        if (ilegalBallOnHole)
        {
            ilegalBallOnHole = false;
            availableHits++;
        }
        isPlayer1 = !isPlayer1;
        if (isPlayer1) Debug.Log("PLAYER 1 TURN");
        else Debug.Log("PLAYER 2 TURN");
    }

    private static void isMyBallType(string ballType, GameObject hole)
    {
        if (isPlayer1)
        {
            if (player1BallType == -1)
            {
                player1BallType = ballType.Equals("SolidBall") ? SOLID : STRIPPED; 
                player2BallType = ballType.Equals("SolidBall") ? STRIPPED : SOLID;
                player1Balls--;
                availableHits++;
            }
            else if (player1BallType == STRIPPED && ballType.Equals("SolidBall") ||
                player1BallType == SOLID && ballType.Equals("StrippedBall"))
            {
                ilegalBallOnHole = true;
                player2Balls--;
                isLastBall(hole);
            }
            else
            {
                player1Balls--;
                availableHits++;
                isLastBall(hole);
            }
        }
        else
        {
            if (player2BallType == -1)
            {
                player2BallType = ballType.Equals("SolidBall") ? SOLID : STRIPPED;
                player1BallType = ballType.Equals("SolidBall") ? STRIPPED : SOLID;
                player2Balls--;
                availableHits++;
            }
            else if (player2BallType == STRIPPED && ballType.Equals("SolidBall") ||
                player2BallType == SOLID && ballType.Equals("StrippedBall"))
            {
                ilegalBallOnHole = true;
                player1Balls--;
                isLastBall(hole);
            }
            else
            {
                availableHits++;
                player2Balls--;
                isLastBall(hole);
            }
        }
    }

    private static void isLastBall(GameObject hole)
    {
        int holeNumber = int.Parse(Regex.Match(hole.tag, @"\d+").Value);
        if (player1Balls == 0 && player1FinalHole == NONE)
        {
            player1FinalHole = holeNumber < 4 ? holeNumber + 3 : holeNumber - 3;
            ParticleSystem finalHole = GameObject.FindGameObjectWithTag("Hole" + player1FinalHole).GetComponentInChildren<ParticleSystem>();
            if (finalHole.isPlaying)
            {
                finalHole.startColor = Color.yellow;
            }
            else
            {
                finalHole.startColor = Color.blue;
                finalHole.Play();
            }
        }
        else if (player2Balls == 0 && player2FinalHole == NONE)
        {
            player2FinalHole = holeNumber < 4 ? holeNumber + 3 : holeNumber - 3;
            ParticleSystem finalHole = GameObject.FindGameObjectWithTag("Hole" + player2FinalHole).GetComponentInChildren<ParticleSystem>();
            if (finalHole.isPlaying)
            {
                finalHole.startColor = Color.yellow;
            }
            else
            {
                finalHole.startColor = Color.green;
                finalHole.Play();
            }
        }
    }

    private static void endOfGame(GameObject hole)
    {
        int holeNumber = int.Parse(Regex.Match(hole.tag, @"\d+").Value);
        if (isPlayer1)
        {
            if (player1Balls == 0 && holeNumber == player1FinalHole) player1Win = true;
            else player2Win = true;
        }
        else
        {
            if (player2Balls == 0 && holeNumber == player2FinalHole) player2Win = true;
            else player1Win = true;
        }
    }
}
