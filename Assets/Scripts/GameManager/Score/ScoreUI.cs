﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour {

    public GameObject Player1Fields;
    public GameObject Player2Fields;

    public Text turnP1;
    public Text BallTypeP1;
    public Text RemainingBallsP1;
    public Text winP1;

    public Text turnP2;
    public Text BallTypeP2;
    public Text RemainingBallsP2;
    public Text winP2;
	
	void Update () {
        refreshActivePlayer();
        refreshTextFields();
        somePlayerWon();
	}

    private void refreshActivePlayer()
    {
        if (ScoreManager.isPlayer1)
        {
            Player1Fields.SetActive(true);
            Player2Fields.SetActive(false);
        }
        else
        {
            Player1Fields.SetActive(false);
            Player2Fields.SetActive(true);
        }
    }

    private void refreshTextFields()
    {
        turnP1.text = "PLAYER 1: " + ScoreManager.availableHits;
        BallTypeP1.text = getBallType(1)[0];
        RemainingBallsP1.text = "R. BALLS: " + ScoreManager.player1Balls;

        turnP2.text = "PLAYER 2: " + ScoreManager.availableHits;
        BallTypeP2.text = getBallType(1)[1];
        RemainingBallsP2.text = "R. BALLS: " + ScoreManager.player2Balls;
    }

    private string[] getBallType(int player)
    {
        string[] ballTypes = new string[2];
        if (ScoreManager.player1BallType.Equals(ScoreManager.NONE))
        {
            ballTypes[0] = " - ";
            ballTypes[1] = " - ";
        }
        else if (ScoreManager.player1BallType.Equals(ScoreManager.SOLID))
        {
            ballTypes[0] = " SOLID ";
            ballTypes[1] = " STRIPPED ";
        }
        else
        {
            ballTypes[0] = " STRIPPED ";
            ballTypes[1] = " SOLID ";
        }
        return ballTypes;
    }

    private void somePlayerWon()
    {
        if (ScoreManager.player1Win)
        {
            Player1Fields.SetActive(true);
            Player2Fields.SetActive(true);
            this.winP1.text = "WIN !!!";
            this.winP2.text = "LOSE !!!";
            Time.timeScale = 0f;
        }
        else if (ScoreManager.player2Win)
        {
            Player1Fields.SetActive(true);
            Player2Fields.SetActive(true);
            this.winP2.text = "WIN !!!";
            this.winP1.text = "LOSE !!!";
            Time.timeScale = 0f;
        }
        else
        {
            this.winP2.text = "";
            this.winP1.text = "";
            Time.timeScale = 1f;
        }
    }
}
