﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickController : MonoBehaviour {

    private Vector3 dirForce = new Vector3();
    private float valForce = 0;
    private float maxValForce = 90f; //Max=100f

    public GameObject stick;
    public GameObject stickEnd;   
    private Rigidbody rstick;
    private Transform pivot;

    public GameObject ball;
    private Rigidbody rball;
    private Transform target;

    private bool canHit = true;
    
    void Start()
    {
        this.rstick = this.GetComponent<Rigidbody>();
        this.pivot = new GameObject().transform;
        stick.transform.parent = pivot;

        this.rball = this.ball.GetComponent<Rigidbody>();
        this.target = this.ball.transform;
    }

    
    void Update()
    {
        stickAroundBall();
        updateDirectionForce();
        
        if (Input.GetMouseButton(0) && canHit)
        {
            updateValueForce();
        }

        if (Input.GetMouseButtonUp(0))
        {
            showStick(false);
            rball.velocity = dirForce * valForce;
            valForce = 0;
            dirForce = new Vector3();
        }

        if (Input.GetMouseButtonDown(1) && !canHit)
        {
            showStick(true);
            ScoreManager.checkTurn();
        }

        //***************** SPEND TURN **************************
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ScoreManager.checkTurn();
        }
        //***************** SPEND TURN **************************
    }

    private void stickAroundBall()
    {
        // Position from Ball respect to camera (Tagged with MainCamera tag)
        Vector3 v3Pos = Camera.main.WorldToScreenPoint(target.position);
        // Calculate difference vector from Mouse position and Ball position
        v3Pos = Input.mousePosition - v3Pos;
        // Angle in screen plane
        float angle = Mathf.Atan2(v3Pos.x, v3Pos.y) * Mathf.Rad2Deg;
        // Stablish position and rotation from stick respect to the ball (target)
        pivot.position = target.position;
        pivot.rotation = Quaternion.AngleAxis(angle, Vector3.up);
    }


    private void showStick(bool show) 
    {
        stick.GetComponent<CapsuleCollider>().enabled = show;
        stick.GetComponent<MeshRenderer>().enabled = show;
        stickEnd.GetComponent<MeshRenderer>().enabled = show;
        canHit = show;
    }

    private void updateDirectionForce()
    {
        Vector3 v3Pos = Camera.main.WorldToScreenPoint(target.position);
        v3Pos = Input.mousePosition - v3Pos;
        if (Mathf.Abs(v3Pos.x) > Mathf.Abs(v3Pos.y))
        {
            dirForce = new Vector3(-v3Pos.y / Mathf.Abs(v3Pos.x), 0f, v3Pos.x / Mathf.Abs(v3Pos.x));
        } 
        else
        {
            dirForce = new Vector3(-v3Pos.y / Mathf.Abs(v3Pos.y), 0f, v3Pos.x / Mathf.Abs(v3Pos.y));
        }
    }

    private void updateValueForce()
    {
        if (valForce >= maxValForce) valForce = maxValForce;
        else
        {
            valForce += Time.deltaTime * 500;
        }
    }

    /*
    private void DrawLine()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            // Find the line from the gun to the point that was clicked.
            Vector3 incomingVec = hit.point - target.position;

            // Use the point's normal to calculate the reflection vector.
            Vector3 reflectVec = Vector3.Reflect(incomingVec, hit.normal);

            // Draw lines to show the incoming "beam" and the reflection.
            Debug.DrawLine(target.position, hit.point, Color.red);
            Debug.DrawRay(hit.point, reflectVec, Color.green);
        }
    }
    */
}
