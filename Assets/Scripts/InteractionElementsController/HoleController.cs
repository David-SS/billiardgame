﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleController : MonoBehaviour {

    private void OnTriggerEnter(Collider ball)
    {
        ScoreManager.ballOnHole(ball.tag, this.gameObject);
    }

}
